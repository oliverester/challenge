require(data.table)
require(fasttime)

loadCsvsToDT <- function(inputFolder="./data/csv_files/", 
                         outputFolder,
                         outputFilename,
                         sampleSize=NULL) {
  
  csvFilePaths = dir(path = inputFolder, pattern = "\\.csv$", full.names = TRUE, recursive = FALSE)
  
  if(!is.null(sampleSize)){
    cat("Using sample mode. Number of files: ", sampleSize, "\n")
    csvFilePaths = csvFilePaths[1:sampleSize] 
  }
  
  list_of_datatables <- lapply(X=csvFilePaths, FUN=fread_cat, sep=",")
  tripdata <- rbindlist(list_of_datatables)
  
  setnames(tripdata, gsub(" ", "_", names(tripdata)))

  tripdata[, starttime_conv:=as.POSIXct(starttime, tz='EST')]
  tripdata[, stoptime_conv:=as.POSIXct(stoptime, tz='EST')]
  tripdata[, start_station_name:=as.factor(start_station_name)]
  tripdata[, end_station_name:=as.factor(end_station_name)]
  tripdata[, start_station_id:=factor(start_station_id)]
  tripdata[, end_station_id:=factor(end_station_id)]
  
  tripdata[, usertype:=as.factor(usertype)]
  tripdata[, gender:=as.factor(gender)]
  
  #create out folder if not exists
  dir.create(outputFolder, showWarnings = FALSE)
  saveRDS(object = tripdata, 
          file=file.path(outputFolder, outputFilename))
  cat("Saved transactions to ", file.path(outputFolder, outputFilename), "\n")
}


fread_cat <- function(...){
  require(data.table)
  param = list(...)
  cat("Loading file", param[[1]], "...\n")
  fread(...)
}
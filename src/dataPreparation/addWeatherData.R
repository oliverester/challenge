require(data.table)
require(fasttime)
require(zoo)

# http://newa.cornell.edu/index.php?page=hourly-weather
WEATHER_CSV_PATH =  "./data/new_york_central_park_weather_2018.csv"

addWeatherData <- function(inputPath="./data/output_load_csvs_to_dt/sample_csv.f",
                           outputFolder="./data/output_transactions_with_weather",
                           outputFilename="sample_trans_w_weather.f") {

  transactionData = readRDS(file = inputPath)
  cat("Adding weather data to", nrow(transactionData), "trips \n")
  
  weatherData2018 = data.table(read.csv2(WEATHER_CSV_PATH))
  weatherData2018[, date_conv:=as.POSIXct(date, format="%m/%d/%Y %H:%M", tz="EST")]
  weatherData2018 = weatherData2018[order(date_conv)]
  weatherData2018[, temp:=as.numeric(temp)]
  weatherData2018[, prec:=as.numeric(prec)]

  # fill up missing precipitation value
  weatherData2018[, prec:=na.locf(prec)]
  weatherData2018[, dewpoint:=NULL]
  
  weatherData2018[, wind:=NULL]
  weatherData2018[, wind_dg:=NULL]
  
  # truncate starttime to hour to join with weather data
  transactionData[, starttime_hour:=as.POSIXct(trunc(starttime_conv, unit="hours"))]

  # left join
  transactionDataWithWeather <- merge(x = transactionData, 
                                      y = weatherData2018, 
                                      by.x = "starttime_hour", 
                                      by.y = "date_conv",
                                      all.x = TRUE)

  matchRatio = round(table(is.na(transactionDataWithWeather$temp))["FALSE"] / 
  sum(table(is.na(transactionDataWithWeather$temp))) * 100, digits = 2) 
  cat("Matched weather for", matchRatio, "% of transactions \n")

  dir.create(outputFolder, showWarnings = FALSE)
  saveRDS(object=transactionDataWithWeather, 
          file=file.path(outputFolder, outputFilename))
  cat("Saved transaction with weather to ", file.path(outputFolder, outputFilename), "\n")
  
}



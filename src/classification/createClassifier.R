require(xgboost)
require(data.table)
require(mlflow)
require(ggplot2)
require(PRROC)
require(precrec)
require(pROC)

source(file = "src/classification/helperFunction.R")
source(file = "src/classification/randomForestLearner.R")
source(file = "src/classification/xgBoostLearner.R")


createClassifier <- function(config, transactions=NA) {

  mlflow_set_tracking_uri("http://127.0.0.1:5000")
  # set mlflow experiment
  mlflow_set_experiment(experiment_name = "CitiBike4", 
                        artifact_location = file.path(getwd(), "mlflow"))
  # start ml flow run
  with(mlflow_start_run(), {
    
    # start ml pipeline
    runPipeline(config, transactions)

  })
}

runPipeline <- function(config, transactions){
  
  cat("Config: \n")
  print(config)
  mlflow_log_batch(params=config2KeyValueDf(config))
  
  if(is.null(transactions)){
    cat("Loading transactions\n")
    transactions = readRDS(config[["inputPath"]])
  }
  # sample
  sampleSize = config[["sampleSize"]]
  if(!is.null(sampleSize)){
    cat("Using sample mode. Selecting", sampleSize, "trip samples. \n")
    transactions = transactions[sample(nrow(transactions), sampleSize)]
  }
  
  featureDataSet = createFeatureSet(rawDataSet = transactions)
  mlflow_log_param(key = "totalNumberRows", 
                   value=nrow(featureDataSet))
  
  selectedLeanerFunc <- get(config[["learner"]])
  # build / cv
  cvRes <- cvLearner(learnerFunc = selectedLeanerFunc,
                     trainSet = featureDataSet,
                     config)
  
  evalCVRes <- evaluateCVClassifier(cvRes = cvRes)
  
  #save result
  logResults(cvRes, evalCVRes)
  
}

logResults <- function(cvRes, evalCVRes) {
  
  cat("Logging model evalution \n")
  # log metrics
  mlflow_log_metric("mean-AUC", evalCVRes$meanAuc)
  mlflow_log_metric("std-AUC", evalCVRes$sdAuc)
  mlflow_log_metric("mean-PR-AUC", evalCVRes$meanPrAuc)
  mlflow_log_metric("std-PR-AUC", evalCVRes$sdPrAuc)
  
  #log fold metrics
  for (idx in 1:length(evalCVRes$auc)){
    cat("log Fold", idx, "\n")
    mlflow_log_metric("Fold-PR-AUC", evalCVRes$prAuc[idx], step=idx)
    mlflow_log_metric("Fold-AUC", evalCVRes$auc[idx], step=idx)
  }
  
  # log first cv model
  mlflowLogModel(model=cvRes[[1]])
  
  # log auc chart
  mlflowLogPlot(p=evalCVRes$rocViz,
                type="auc")
  
  # log feature importance chart
  mlflowLogPlot(p=evalCVRes$featureImportanceViz, 
                type="feature_importance")
}

mlflowLogModel <- function(model){
  modelPath = file.path(getwd(), "tmp/model.RDS")
  saveRDS(object = model, file = modelPath)
  mlflow::mlflow_log_artifact(modelPath)
}

mlflowLogPlot <- function(p, type, height=9, width=5){
  plotPath = file.path(getwd(), paste0("tmp/", type, ".png"))
    png(file = plotPath,
        width=800, height=800, res = 150, units = "px")
    print(p)
    dev.off()
  mlflow::mlflow_log_artifact(plotPath)
}

createFeatureSet <- function(rawDataSet){
  
  #rename target
  setnames(rawDataSet, old=config[["targetCol"]], new="target")
  targetCol="target"
  
  # select feature
  transactionSelectedFeature = rawDataSet[,c(config[["catVar"]], 
                                             config[["numVar"]],
                                             config[["bolVar"]],
                                             "target"), 
                                            with=F]
  
  # convert label to numeric: 
  cat("Converting Target: Customer -> 0, Subscriber -> 1 \n")
  transactionSelectedFeature[target=="Customer",targetNum:=1]
  transactionSelectedFeature[target=="Subscriber",targetNum:=0]
  transactionSelectedFeature[,target:=NULL]
  setnames(transactionSelectedFeature, old="targetNum", new="target")
  
  # one-hot-encoding for cat variables
  if(config[["oneHotEncode"]]){
    cat("One-Hot-Encoding feature \n")
    transactionSelectedFeature = oneHotEncode(dataSet=transactionSelectedFeature, 
                                      columns=c(config[["catVar"]],
                                                config[["bolVar"]]))
  }

  return(transactionSelectedFeature)
}


oneHotEncode <- function(dataSet, 
                         columns){
  
  require(mltools)
  
  # convert to factor vars
  dataSetFactor <- dataSet[, (columns):=lapply(.SD, as.factor), 
                           .SDcols=columns]
  
  dummyDataSet <- one_hot(dataSet[,columns, with=F])
  # combine one hot set with rest of table
  finalOneHotDataSet <- cbind(dataSet[,-columns, with=F] , dummyDataSet)
  return(finalOneHotDataSet)
}


cvLearner <- function(learnerFunc,
                      trainSet,
                      config) {

  foldSets = folds(trainSet, nfolds = config[["nFolds"]])
  
  modelResult <- list()
  
  # iterate folds
  for (foldId in 1:length(foldSets)) {
    cat("Training for fold ", foldId, "\n")
    
    currentTrainSet <- rbindlist(foldSets[-foldId])
    dist = table(currentTrainSet[,target])
    cat("Fold", foldId, ": Train Target Distribution", paste(paste(paste0(names(dist),":"), dist),collapse = ", "), "\n")

    currentValiSet <- foldSets[[foldId]]
    dist = table(currentValiSet[,target])
    cat("Fold", foldId, ": Vali Target Distribution", paste(paste(paste0(names(dist),":"), dist),collapse = ", "), "\n")
    
    # call learner func
    modelRes <- learnerFunc(dataSet=currentTrainSet, 
                            config=config)
    modelResult[[foldId]] <- modelRes
    modelResult[[foldId]]$testPred <- modelRes$predict(predictData = currentValiSet)
  }
  
  return(modelResult)
}



evaluateCVClassifier <- function(cvRes){
  
  
  cvEval <- list()
  
  prViz <- c()
  aucViz <- c()
  aucVec <- c()
  rocVec <- c()
  prVec <- c()
  confMatrix <- c()
  meanAUC <- c()
  evalModDT <- data.table(scores=numeric(),
                          labels=numeric(),
                          modnames=character())
  
  featureImportance <- data.frame('fold'=numeric(),
                                  'feature'=character(), 
                                  'importance'=numeric())

  for( idx in 1:length(cvRes)) {
    res <- cvRes[[idx]]
    # con matrix
    predicted = as.numeric(res$testPred$pred > 0.5)
    confMatrix[[idx]] <- table("pred"=predicted , 
                               "actual"=res$testPred$actual)
    
    # determine ROC & PRC AUCs
    evalModDT = rbind(evalModDT, data.table(scores = res$testPred$pred, 
                                            labels = res$testPred$actual,
                                            modnames= as.character(idx)))
    sscurves <- evalmod(scores = res$testPred$pred, 
                        labels = res$testPred$actual)
    scores = as.data.table(precrec::auc(sscurves))
    # roc auc
    aucVec[idx] <- as.numeric(scores[curvetypes=='ROC', aucs])
    
    prVec[idx] <- as.numeric(scores[curvetypes=='PRC', aucs])

    #feature importance
    featureImportance = rbind(featureImportance, cbind('fold'=idx, res$featureImportance))
  }

  curv <- evalmod(nfold_df = as.data.frame(evalModDT), 
                  score_cols = c(1),
                  lab_col = 2,
                  fold_col = 3, 
                  raw_curves = TRUE)
  autoplot(curv, show_cb = FALSE)
  rocViz <- recordPlot()

  featureImportanceViz = getFeatureImportanceViz(featureImportance)
  
  cvEval$featureImportanceViz <- featureImportanceViz
  cvEval$rocViz <- rocViz
  cvEval$auc <- aucVec
  cvEval$prAuc <- prVec
  cvEval$meanAuc <- mean(aucVec)
  cvEval$sdAuc <- sd(aucVec)
  cvEval$meanPrAuc <- mean(prVec)
  cvEval$sdPrAuc <- sd(prVec)
  cvEval$roc <- rocVec
  cvEval$confusionMatrices <- confMatrix
  print(cvEval)
  return(cvEval)
}

getFeatureImportanceViz <- function(featureImportance){
  
  # create feature importance plot
  featureImportanceAggregation = featureImportance[,.(meanImportance=mean(importance),
                                                      sdImportance=sd(importance)), by=feature]
  featureImportanceAggregation = featureImportanceAggregation[order(-meanImportance)][1:min(20, nrow(featureImportanceAggregation))]
  g <- ggplot(featureImportanceAggregation, aes(x=reorder(feature,-meanImportance), y=meanImportance)) + 
       geom_bar(stat='identity') +
       geom_errorbar(aes(ymin=meanImportance-sdImportance, ymax=meanImportance+sdImportance), width=.2,
                  position=position_dodge(.9)) +
       theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
       ggtitle("Top-20 Feature Importance")
  return(g)
}


source("src/classification/createClassifier.R")

#[1] "start_station_longitude"      "start_station_latitude"       "end_station_longitude"       
#[4] "end_station_latitude"         "starttime_hour"               "tripduration"                
#[7] "starttime"                    "stoptime"                     "start_station_id"            
#[10] "start_station_name"           "end_station_id"               "end_station_name"            
#[13] "bikeid"                       "usertype"                     "birth_year"                  
#[16] "gender"                       "starttime_conv"               "stoptime_conv"               
#[19] "date"                         "temp"                         "dewpoint"                    
#[22] "prec"                         "rh"                           "wind"                        
#[25] "wind_dg"                      "expectedDuration"             "expectedDistance"            
#[28] "start_day"                    "start_month"                  "start_date"                  
#[31] "start_weekday"                "start_hour"                   "stop_day"                    
#[34] "stop_month"                   "stop_date"                    "stop_weekday"                
#[37] "stop_hour"                    "tripduration_minutes"         "tripduration_hours"          
#[40] "is_roundtrip"                 "ratio_real_expected_duration" "speed"                       
#[43] "mean_last_5_hours_temp"       "sum_last_5_hours_prec"       

# main
config = list()

config[["inputPath"]] = "./data/output_transactions_with_feature/trans_w_feature.f"
config[["targetCol"]] = "usertype"

# define variables to train classifier with
config[["catVar"]] = c(#"start_station_id", 
                       #"birth_year", 
                       #"gender",
                      "start_day", 
                      "start_month",
                      "start_hour", 
                      "start_weekday")

config[["numVar"]] = c("start_station_latitude",
                       "start_station_longitude",
                       "end_station_latitude",
                       "end_station_longitude",
                       "tripduration",
                       "ratio_real_expected_duration", 
                       "expectedDuration",
                       "expectedDistance",
                       "speed",
                       "mean_last_5_hours_temp",
                       "sum_last_5_hours_prec",
                       "prec")

config[["bolVar"]] = c("is_roundtrip")
config[["oneHotEncode"]] = TRUE
config[["trainValiSplit"]] = 0.8

config[["downsample"]] = FALSE

config[["nFolds"]] = 2
config[["xgboost"]][["nround"]] = 500
config[["xgboost"]][["eta"]] = 0.1
config[["xgboost"]][["scaleClasses"]] = FALSE
config[["xgboost"]][["max_depth"]] = 3
config[["xgboost"]][["subsample"]] = 0.75
config[["xgboost"]][["colsample_bytree"]] = 0.75

config[["sampleSize"]] = 500000
config[["learner"]] = 'xgBoostLearner'

#config[["randomforest"]][["ntrees"]] = c(50,100,200,500) 

#transactions = readRDS(config[["inputPath"]])
#transactionsSample = transactions[sample(nrow(transactions), config[["sampleSize"]])]
#saveRDS(transactionsSample, "t2")
#transactionsSample = readRDS("t2")
createClassifier(config=config, 
                 transactions = transactionsSample)


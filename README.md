---
title: "README"
author: "Oliver Ester"
date: "23 2 2021"
output: html_document
---


## CitiBike Analyse

Das Projekt untersucht das Nutzerverhalten von NY CitiBikes vom Jahr 2018.
Die Nutzerdaten liegen in anonymisierter Form öffentlich zugänglich vor.

### Ordner src

Der src-Ordner stellt zwei main-Scripte bereit.
mainDataPreparation.R ist die Schnittstelle, um die Data-Preparation-Pipeline 
auszuführen. Im Ordner dataPrepartion werden dazu je Pipeline-Schritt weitere 
Funktionen bereitgestellt.
mainClassification.R ist die Schnittstelle, um Classifkations-Läufe zu starten. 
Der Ordner classification stell dazu weitere R-Scripte bereit.

### Ordner data

Der data-Ordner enthält alle Rohdaten (Wetter, CitiBike-CSVs). Ebenso werden 
hier die einzelnen Data-Preparation-Stages zwischengespeichert. Final wird ein
Datenset für die Classification bereitgestellt.

### Ordner exploration

Der exploration-Ordner analysiert/visualisiert das Nutzerverhalten. Dazu greift
die Analyse auf die aufbereiteten Daten im Ordner data zurück.

### Ordner osrm

Hier liegt alles für die Nutzung eines lokalen OSRM-Servers bereit.

### Ordner mlflow

Hier werden die Experiment-Läufe durch mlflow gespeichert.

### Ordner misc

Hier liegt alles andere.
